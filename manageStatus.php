<?php

  require 'database.php';

  class ManageStatus {
      private static $pdo = '';

      public function __construct() {
          self::$pdo = Database::connect();
          self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      }

      public function get_statuses(){
          $sql = "SELECT COL_ID FROM status_columns where COL_STATUS = 1";
          $q = self::$pdo->prepare($sql);
          $q->execute();
          return json_encode($q->fetchAll(PDO::FETCH_COLUMN, 0));
      }

      public function update_status($col_id){
          return self::$pdo->prepare("update status_columns set COL_STATUS=1 where COL_ID={$col_id}")->execute();
      }

      public function remove_status($remove_col_id){
          return self::$pdo->prepare("update status_columns set COL_STATUS=0 where COL_ID={$remove_col_id}")->execute();
      }

      public function __destruct() {
          self::$pdo = '';
          Database::disconnect();
      }
  }

  $manageStatus = new ManageStatus;
  if(isset($_POST['col_id'])){
    echo $manageStatus->update_status($_POST['col_id']);
  }elseif(isset($_POST['remove_col_id'])){
    echo $manageStatus->remove_status($_POST['remove_col_id']);
  }else{
    echo $manageStatus->get_statuses();
  }
