<!DOCTYPE HTML>

<html lang="en">
    <!DOCTYPE HTML>

<html lang="en">
<header>
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

  <link rel="stylesheet" href="css/custom.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <div class="d-flex align-items-center">
      <h1 id="jhhbh">Trello mock-up system</h1>
      <div class="pull-rightv">
        <button class="add-status-btn btn btn-secondary" onclick="showAddStatus()" >Manage Status</button>
        <button class="create-task-btn btn btn-secondary" onclick="showCreateCard()" >Create Card</button>
      </div>
    </div>
</header>

<body>

<div class="main-container container-fluid">
  <ul class="columns" id="statuses-column">
  </ul>
</div>

<ul class="task-container columns">
</ul>

</body>
<?php
  // require 'manageStatus.php';
  // require 'manageCards.php';

 ?>

<div class="modal-dialog create-card" role="document">
  <div class="modal-content">
    <div class="modal-header text-center">
      <h4 class="modal-title w-100 font-weight-bold">Create Card</h4>
      <button type="button" class="close" onclick="closeModal()" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body mx-3" id="card-modal">
      <div class="md-form mb-5">
        <i class="fas fa-user prefix grey-text"></i>
        <label data-error="wrong" data-success="right" for="form35">Title</label>
        <input type="text" name="create_card_title" id="create-card-title" value="" class="form-control validate">
        </input>
      </div>
    </div>
    <div class="modal-footer d-flex justify-content-center">
      <button id="submitCard" class="btn btn-unique">Create <i class="fas fa-paper-plane-o ml-1"></i></button>
    </div>
  </div>
</div>

<div class="modal-dialog add-task-status" role="document">
  <div class="modal-content">
    <div class="modal-header text-center">
      <h4 class="modal-title w-100 font-weight-bold">Add status</h4>
      <button type="button" class="close" onclick="closeModal()" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body mx-3" id="status-modal">
      <div class="md-form mb-5">
        <i class="fas fa-user prefix grey-text"></i>
        <label data-error="wrong" data-success="right" for="form34">Status</label>
        <select type="text" name="task_status_field" id="ftask_status_field" class="form-control validate">
          <option value="0">To Do</option>
          <option value="1">Doing</option>
          <option value="2">Delayed</option>
          <option value="3">Done</option>
        </select>
      </div>
    </div>
    <div class="modal-footer d-flex justify-content-center">
      <button id="remove-card-status"  class="btn btn-danger" >Remove <i class="fas fa-paper-plane-o ml-1"></i></button>
      <button id="submit"  class="btn btn-success" >ADD <i class="fas fa-paper-plane-o ml-1"></i></button>
    </div>
  </div>
</div>



</body>
<script src="js/custom.js"></script>
</html>

</html>
