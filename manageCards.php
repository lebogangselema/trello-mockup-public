<?php

  require 'database.php';

  class ManageCards {
      private static $pdo = '';

      public function __construct() {
          self::$pdo = Database::connect();
          self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      }

      public function get_cards(){
          $sql = "SELECT CARD_HTML_ID, TITLE, CARD_STATUS_ID FROM cards";
          $q = self::$pdo->prepare($sql);
          $q->execute();
          return json_encode($q->fetchAll());
      }

      public function create_card($card_title, $card_html_id){
          $sql = "INSERT INTO  cards (TITLE, CARD_STATUS, CARD_STATUS_ID, CARD_HTML_ID) VALUES ('"
             . $card_title . "', 0, 'to-do', '" . $card_html_id . "')";
          return self::$pdo->prepare($sql)->execute();
      }

      public function update_card($card_id, $card_status){
        return self::$pdo->prepare("UPDATE cards SET `CARD_STATUS_ID`='{$card_status}' WHERE `CARD_HTML_ID`='{$card_id}'")->execute();
      }

      public function __destruct() {
          self::$pdo = '';
          Database::disconnect();
      }
  }

  $manageCards = new ManageCards;
  if(isset($_POST['card_id'])){
    echo $manageCards->update_card(trim($_POST['card_id']), $_POST['card_status']);
  }elseif(isset($_POST['card_title'])){
    echo $manageCards->create_card($_POST['card_title'], $_POST['card_html_id']);
  }else{
    echo $manageCards->get_cards();
  }
