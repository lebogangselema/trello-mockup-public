-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 21, 2020 at 10:42 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trello_mock_up`
--

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--
-- Creation: Feb 20, 2020 at 10:04 PM
--

CREATE TABLE `cards` (
  `CARD_ID` int(11) NOT NULL,
  `TITLE` varchar(100) COLLATE utf8_bin NOT NULL,
  `CARD_STATUS` int(11) NOT NULL,
  `DATE_CREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DATE_UPDATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CARD_STATUS_ID` varchar(20) COLLATE utf8_bin NOT NULL,
  `CARD_HTML_ID` varchar(25) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`CARD_ID`, `TITLE`, `CARD_STATUS`, `DATE_CREATED`, `DATE_UPDATED`, `CARD_STATUS_ID`, `CARD_HTML_ID`) VALUES
(13, 'Testing', 0, '2020-02-21 08:38:43', '2020-02-21 08:38:43', 'to-do', '5blwQObbAOI0lnjp9Jfa'),
(14, 'Tester', 0, '2020-02-21 08:39:31', '2020-02-21 08:39:31', 'to-do', 'mgB6svp13DAm5w0cJXom');

-- --------------------------------------------------------

--
-- Table structure for table `status_columns`
--
-- Creation: Feb 20, 2020 at 10:03 PM
--

CREATE TABLE `status_columns` (
  `COL_ID` int(11) NOT NULL,
  `COL_NAME` varchar(20) COLLATE utf8_bin NOT NULL,
  `COL_STATUS` int(11) NOT NULL,
  `COL_HTML_ID` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `status_columns`
--

INSERT INTO `status_columns` (`COL_ID`, `COL_NAME`, `COL_STATUS`, `COL_HTML_ID`) VALUES
(0, 'To do', 1, 'to-do'),
(1, 'Doing', 0, 'doing'),
(2, 'Delayed', 0, 'delayed'),
(3, 'Done', 0, 'done');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`CARD_ID`);

--
-- Indexes for table `status_columns`
--
ALTER TABLE `status_columns`
  ADD PRIMARY KEY (`COL_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `CARD_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
