

window.addEventListener("DOMContentLoaded", function(){
  fetch('manageStatus.php').then(function(response){
      /*
       *Return colum statuses database
       */
      var ourStatusRequest = new XMLHttpRequest();
      ourStatusRequest.open('GET', 'manageStatus.php', true);
      ourStatusRequest.responseType = 'json';
      ourStatusRequest.onload = function(){
         var ourStatusRequests = this.response;
         for(var cardStatus in ourStatusRequests){
           addTaskStatus(ourStatusRequests[cardStatus].toString());
         }
      }
      ourStatusRequest.send();
      return fetch('manageCards.php');
  }).then(function(responseTwo){
      /*
       *Return cards from database
       */
      var ourCardsRequest = new XMLHttpRequest();
      ourCardsRequest.open('GET', 'manageCards.php', true);
      ourCardsRequest.responseType = 'json';
      ourCardsRequest.onload = function(){
         var ourCardsRequest = jQuery.parseJSON(JSON.stringify(this.response));
           for (let card of ourCardsRequest) {
              loadCard(card.TITLE, card.CARD_HTML_ID, card.CARD_STATUS_ID);
           }
      }
      ourCardsRequest.send();
  }).catch(function(err){
    console.log(err);
        // handle errors from all the steps above at once
  });

  (function($){
    $(function(){
      $(document).on('click', '#submit', function(a){
        a.preventDefault;
        $.ajax({
          method: 'POST',
          url: 'manageStatus.php',
          dataType: 'html',
          data: {
            'col_id': $('#status-modal').find('select option:selected').val(),
          },
          success: function(r) {
            if(r){
              addTaskStatus($('#status-modal').find('select option:selected').val());
            }
          }
        });
      });

      $(document).on('click', '#remove-card-status', function(a){
        if (!window.confirm("Do you really want to remove this?")){
          $('.add-task-status').css('display', 'none');
          return;
        }
        a.preventDefault;
        $.ajax({
          method: 'POST',
          url: 'manageStatus.php',
          dataType: 'html',
          data: {
            'remove_col_id': $('#status-modal').find('select option:selected').val(),
          },
          success: function(r) {
            if(r){
              removeHTMLColumn($('#status-modal').find('select option:selected').val());
            }
          }
        });
      });

      $(document).on('click', '#submitCard', function(a){
        a.preventDefault;
        let cardHtmlId = htmlCardID();
        $.ajax({
          method: 'POST',
          url: 'manageCards.php',
          dataType: 'html',
          data: {
            'card_title': $('#card-modal').find(':input').val(),
            'card_html_id': cardHtmlId,
          },
          success: function(r) {
            if(r){
               createCard($('#card-modal').find(':input').val(), cardHtmlId);
            }
          }
        });
      });
    });
  })($);
});

const closeModal = () => {
  $('.add-task-status').css('display', 'none');
  $('.create-card').css('display', 'none');
}

const removeHTMLColumn = (value) => {
  if (value === '0'){
      $('#to-do').css('display', 'none');
      $('.to-do-column').css('display', 'none');
  }else if (value === '1') {
      $('#doing').css('display', 'none');
      $('.doing-column').css('display', 'none');
  }else if (value === '2') {
      $('#delayed').css('display', 'none');
      $('.delayed-column').css('display', 'none');
  }else{
      $('#done').css('display', 'none');
      $('.done-column').css('display', 'none');
  }
  $('.add-task-status').css('display', 'none');
}

const createHTMLColumn = (value) => {
  if (value === '0'){
    var appendClass = 'to-do-column bg-warning';
    var status = 'To Do';
  }else if (value === '1') {
    var appendClass = 'doing-column bg-primary';
    var status = 'Doing';
  }else if (value === '2') {
    var appendClass = 'delayed-column bg-danger';
    var status = 'Delayed';
  }else{
    var appendClass = 'done-column bg-success';
    var status = 'Done';
  }
  return '<li class="column ' + appendClass + '" ' +
    '  ondrop="drop(event, this)" ondragover="allowDrop(event)"> ' +
    '<div class="column-header"><h4>' + status + '</h4></div></li>';
}

const createHTMLCard = (value) => {
  var cardID = 'to-do';
  if (value === '3'){
    cardID = 'done';
  }else if (value === '2') {
    cardID = 'delayed';
  }else if (value === '1') {
    cardID = 'doing';
  }
  return '<li class="task-list" id="'+ cardID + '" ondrop="drop(event, this)" '
    + 'ondragover="allowDrop(event)"></li>';
}

/*
 * This creates status columns for cards
 */
const addTaskStatus = (value) => {
  var status_column;
  var status_container;
  switch (value){
    case '0':
      status_column = createHTMLColumn(value);
      status_container = createHTMLCard(value);
      break;
    case '1':
      status_column = createHTMLColumn(value);
      status_container = createHTMLCard(value);
      break;
    case '2':
      status_column = createHTMLColumn(value);
      status_container = createHTMLCard(value);
      break;
    case '3':
      status_column = createHTMLColumn(value);
      status_container = createHTMLCard(value);
       break;
  }
  document.querySelector('#statuses-column').innerHTML += status_column;
  document.querySelector('.task-container').innerHTML += status_container;
  $('.add-task-status').css('display', 'none');
}

const createCard = (value, cardID) => {
  var newCard = '<div class="task" id="' + cardID + '" draggable="true" ' +
  ' ondragstart="drag(event)"><p>' + value + '<p></div>';

  document.querySelector('#to-do').innerHTML += newCard;
  $('.create-card').css('display', 'none');
}

const loadCard = (title, html_card_id, status) => {
  var newCard = '<div class="task" id="' + html_card_id + '" draggable="true" ' +
  ' ondragstart="drag(event)"><p>' + title + '<p></div>';
  console.log(status);
  if(document.getElementById(status) !== null){
    document.getElementById(status).innerHTML += newCard;
  }
}

const htmlCardID = () => {
  /*
   *Create card ID so we can have diferent card IDs
   */
  var cardID = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var length = characters.length;
  for (var i = 0; i < 20; i++){
    cardID += characters.charAt(Math.floor(Math.random() * length));
  }
  return cardID;
}

const allowDrop = (e) => {
  e.preventDefault();
}

const drag = (e) => {
  e.dataTransfer.setData("text", e.target.id);
  if(e.target.getAttribute("draggable") === "true"){
      e.dataTransfer.dropEffect = "none";
  } else {
      e.dataTransfer.dropEffect = "all";
  }
}

const showAddStatus = () => {
  $('.add-task-status').css('display', 'block');
}

const showCreateCard = () => {
  $('.create-card').css('display', 'block');
  document.getElementById("create-card-title").value = "";
}

var drop = (e, el) => {
  e.preventDefault;
  var data = e.dataTransfer.getData("text");
  document.addEventListener("drop", function(event) {
    $.ajax({
      method: 'POST',
      url: 'manageCards.php',
      dataType: 'html',
      data: {
        'card_id': data,
        'card_status': el.id,
      },
      success: function(r) {
        if(r){
          el.appendChild(document.getElementById(data));
        }
      }
    });
  });
}
